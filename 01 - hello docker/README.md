# Hello Docker

So you run docker, but do you know what's behind a docker *image*?

## Meet nix

Build the image by yourself using nix: `nix build -f hello_docker.nix`, the image is called `result`

Instead of doing `docker pull` you can import the image directly: `docker load < result`

Like a normal docker image this image can be run with `docker run hello_docker:latest` and inspected with `docker image inspect hello_docker:latest`

With this step we can say straight away that this Docker image is made up by only one layer

## Meet docker image format

Because of nix the `result` is a symlink to the nix store, but this doesn't prevent us to notice that a Docker image really is... a tar file!

`file result` really tells that it's a symlink to /nix/store/...tar.gz

If we uncompress it `mkdir -p hello_docker_image; tar xvf result -C hello_docker_image` we notice the actual structure of a docker image:

- a manifest that basically references other resources: all the layers, the image configuration, the name of the image
- various directories with some `layer.tar` files, these are the actual image layers with the content of our image. If you unpack these, remember `--delay-directory-restore`
- the config which contains some image metadata (e.g. what to launch when the image is being run)

## References

1. [OCI Image specification](https://github.com/opencontainers/image-spec)
2. [Docker image specification](https://github.com/moby/moby/tree/master/image/spec)
