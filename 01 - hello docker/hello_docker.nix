{
    sources ? import ../nix/sources.nix,
    pkgs ? import sources.nixpkgs {}
}:
pkgs.dockerTools.buildImage {
  name = "hello_docker";
  tag = "latest";
  created = "now";
  config.Cmd = [ "${pkgs.hello}/bin/hello" ];
}