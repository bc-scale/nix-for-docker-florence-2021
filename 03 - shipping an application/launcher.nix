{ app, redis, writers  }:
let
export_variables = ''
    export REDIS_HOST=localhost:6379
    export SERVER_PORT=8080
    export ENVIRONMENT=development
'';
in
writers.writeBashBin "launch_app" ''
 set -x
 ${export_variables}
 echo "Launching redis in background"
 ${redis}/bin/redis-server --bind 0.0.0.0&
 echo "Launching app in foreground"
 ${app}/bin/greeter
 trap 'kill $(jobs -p)' EXIT
''