{
    sources ? import ./nix/sources.nix,
    pkgs ? import sources.nixpkgs {}
}:
pkgs.mkShell {
    buildInputs = with pkgs; [
        # coreutils and some very basic stuff is implicit - such as tar
        nix # Bring nix with us, we'll need it
        file # Let's inspect things
        docker # Just the client, daemon should already be running
        redis 
    ];
}